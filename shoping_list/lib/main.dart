import 'package:flutter/material.dart';
import 'package:shoping_list/screens/firs_view/first_view.dart';
import 'package:shoping_list/screens/home_view/home_view.dart';
import 'package:shoping_list/screens/home_view/list_view.dart';

import 'package:shoping_list/screens/singUp_In_view/sing_up_view.dart';
import 'package:shoping_list/services/auth_service.dart';

import 'package:shoping_list/widgets/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Provider(
      auth: AuthService(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(),
        home: HomeControler(),
        routes: <String, WidgetBuilder>{
          "/singUp": (BuildContext context) =>
              SingUpView(authFormType: AuthFormType.singUp),
          "/singIn": (BuildContext context) => SingUpView(
                authFormType: AuthFormType.singIn,
              ),
          "/home": (BuildContext context) => HomeControler(),
          "/list": (BuildContext context) => ShoppingListView(),
        },
      ),
    );
  }
}

class HomeControler extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AuthService auth = Provider.of(context).auth;
    return StreamBuilder(
      stream: auth.onAuthStateChanged,
      builder: (context, AsyncSnapshot<String> snapshot) {
        if (snapshot.connectionState == ConnectionState.active) {
          final bool singgedIn = snapshot.hasData;
          return singgedIn ? HomeView() : FirstView();
        }
        return CircularProgressIndicator();
      },
    );
  }
}
