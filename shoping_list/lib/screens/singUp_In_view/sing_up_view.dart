import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shoping_list/widgets/provider.dart';
import 'package:flushbar/flushbar.dart';

final primaryColor = const Color(0xffFABC51);
final grayColor = const Color(0xff858585);
enum AuthFormType { singIn, singUp }

class SingUpView extends StatefulWidget {
  final AuthFormType authFormType;

  SingUpView({Key key, @required this.authFormType}) : super(key: key);
  @override
  _SingUpViewState createState() =>
      _SingUpViewState(authFormType: this.authFormType);
}

class _SingUpViewState extends State<SingUpView> {
  AuthFormType authFormType;

  _SingUpViewState({this.authFormType});

  final formKey = GlobalKey<FormState>();

  String _email, _password, _name;

  void switchFormState(String state) {
    formKey.currentState.reset();
    if (state == "singUp") {
      setState(() {
        authFormType = AuthFormType.singIn;
      });
    } else {
      setState(() {
        authFormType = AuthFormType.singUp;
      });
    }
  }

  void submit() async {
    final form = formKey.currentState;
    form.save();

    try {
      final auth = Provider.of(context).auth;
      if (authFormType == AuthFormType.singIn) {
        FocusScope.of(context).requestFocus(new FocusNode());
        String uid = await auth.signInWithEmailAndPassword(_email, _password);
        print("Singed in with $uid");
        Navigator.of(context).pushReplacementNamed("/home");
      } else {
        FocusScope.of(context).requestFocus(new FocusNode());
        String uid =
            await auth.createUserWithEmailAndPassword(_email, _password, _name);
        print("Singed in with $uid");
        Navigator.of(context).pushReplacementNamed("/home");
      }
    } catch (error) {
      switch (error.code) {
        case "ERROR_INVALID_EMAIL":
          Flushbar(
            title: "Error",
            message: "Your email address appears to be malformed.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        case "ERROR_WRONG_PASSWORD":
          Flushbar(
            title: "Error",
            message: "Your password is wrong.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        case "ERROR_USER_NOT_FOUND":
          Flushbar(
            title: "Error",
            message: "User with this email doesn't exist.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        case "ERROR_USER_DISABLED":
          Flushbar(
            title: "Error",
            message: "User with this email has been disabled.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        case "ERROR_TOO_MANY_REQUESTS":
          Flushbar(
            title: "Error",
            message: "Too many requests. Try again later.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        case "ERROR_OPERATION_NOT_ALLOWED":
          Flushbar(
            title: "Error",
            message: "Signing in with Email and Password is not enabled.",
            duration: Duration(seconds: 3),
          )..show(context);

          break;
        default:
          Flushbar(
            title: "Error",
            message: "An undefined Error happened.",
            duration: Duration(seconds: 3),
          )..show(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        width: _width,
        height: _height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                SvgPicture.asset(
                  "lib/assets/images/rightTop.svg",
                  height: _height * 0.15,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 30.0),
              child: Row(
                children: <Widget>[
                  buildHeaderText(),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 30.0),
              child: Form(
                key: formKey,
                child: Column(
                  children: buildInputs(),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 30.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  buildLoginButton(context, _width),
                ],
              ),
            ),
            SizedBox(height: _height * 0.15),
            Padding(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  buildText(),
                  buildChangeFlatButton(context),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Text buildText() {
    String text;

    if (authFormType == AuthFormType.singUp) {
      text = "Already have an account?";
    } else {
      text = "Dont have an account?";
    }

    return Text(
      text,
      style: TextStyle(
        fontSize: 18,
        color: const Color(0xff858585),
      ),
    );
  }

// Build Error Text

// Build Login Button
  RaisedButton buildLoginButton(BuildContext context, double _width) {
    String _buttonName;

    if (authFormType == AuthFormType.singUp) {
      _buttonName = "SIGN UP";
    } else {
      _buttonName = "LOGIN";
    }
    return RaisedButton(
      onPressed: () {
        submit();
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
      padding: const EdgeInsets.all(0.0),
      child: Ink(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xFFF7C656), const Color(0xFFFEA034)],
          ),
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
        ),
        child: Container(
          constraints: BoxConstraints(
              maxWidth: _width * 0.33,
              minHeight: 46.0), // min sizes for Material buttons
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              AutoSizeText(
                _buttonName,
                maxLines: 1,
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.w500),
              ),
              Icon(
                Icons.arrow_forward,
                color: Colors.white,
                size: 24.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Build Change Button
  FlatButton buildChangeFlatButton(BuildContext context) {
    String _description, _route;

    if (authFormType == AuthFormType.singUp) {
      _description = "Sign In";
      _route = "singUp";
    } else {
      _description = "Sign Up";
      _route = "singIn";
    }
    return FlatButton(
      child: Text(
        _description,
        style: TextStyle(fontSize: 18, color: primaryColor),
      ),
      onPressed: () {
        switchFormState(_route);
        print(_route);
      },
    );
  }

  // Build Header
  Text buildHeaderText() {
    String _haderText;
    if (authFormType == AuthFormType.singUp) {
      _haderText = "Create Account";
    } else {
      _haderText = "Login";
    }
    return Text(
      _haderText,
      style: TextStyle(fontSize: 38),
    );
  }

//build Inputs
  List<Widget> buildInputs() {
    List<Widget> textFields = [];

    if (authFormType == AuthFormType.singUp) {
      textFields.add(
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: TextFormField(
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
            decoration: InputDecoration(
              labelText: "USER NAME",
              labelStyle: TextStyle(color: grayColor),
              prefixIcon: Icon(Icons.person_outline, color: grayColor),
              focusedBorder:
                  OutlineInputBorder(borderSide: BorderSide(color: grayColor)),
            ),
            onSaved: (value) => _name = value,
          ),
        ),
      );
      textFields.add(
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: TextFormField(
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
            decoration: InputDecoration(
              labelText: "EMAIL",
              labelStyle: TextStyle(color: grayColor),
              prefixIcon: Icon(Icons.mail_outline, color: grayColor),
              focusedBorder:
                  OutlineInputBorder(borderSide: BorderSide(color: grayColor)),
            ),
            onSaved: (value) => _email = value,
          ),
        ),
      );
      textFields.add(
        Padding(
          padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
          child: TextFormField(
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
            decoration: InputDecoration(
              labelText: "PASSWORD",
              labelStyle: TextStyle(color: grayColor),
              prefixIcon: Icon(Icons.lock_outline, color: grayColor),
              focusedBorder:
                  OutlineInputBorder(borderSide: BorderSide(color: grayColor)),
            ),
            obscureText: true,
            onSaved: (value) => _password = value,
          ),
        ),
      );
    } else {
      textFields.add(
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: TextFormField(
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
            decoration: InputDecoration(
              labelText: "EMAIL",
              labelStyle: TextStyle(color: grayColor),
              prefixIcon: Icon(Icons.mail_outline, color: grayColor),
              focusedBorder:
                  OutlineInputBorder(borderSide: BorderSide(color: grayColor)),
            ),
            onSaved: (value) => _email = value,
          ),
        ),
      );
      textFields.add(
        Padding(
          padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
          child: TextFormField(
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
            decoration: InputDecoration(
              labelText: "PASSWORD",
              labelStyle: TextStyle(color: grayColor),
              prefixIcon: Icon(Icons.lock_outline, color: grayColor),
              focusedBorder:
                  OutlineInputBorder(borderSide: BorderSide(color: grayColor)),
            ),
            obscureText: true,
            onSaved: (value) => _password = value,
          ),
        ),
      );
    }

    return textFields;
  }

  InputDecoration inputDecoration(String hint) {
    return InputDecoration(
        hintText: hint,
        filled: true,
        fillColor: Colors.transparent,
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent, width: 0.0)),
        contentPadding:
            const EdgeInsets.only(left: 14.0, bottom: 10.0, top: 10.0));
  }
}
