import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_svg/svg.dart';

import 'package:shoping_list/screens/home_view/update_value.dart';
import 'package:shoping_list/services/auth_service.dart';
import 'package:shoping_list/services/firestore_services.dart';
import 'package:shoping_list/widgets/addItem_dialog_box.dart';
import 'package:shoping_list/widgets/addUser_dialog_box.dart';

import 'package:shoping_list/widgets/signOut_dialog_box.dart';

class ShoppingListView extends StatefulWidget {
  final String documentID;
  final String owner;
  final String curentUser;

  const ShoppingListView(
      {Key key, this.documentID, this.owner, this.curentUser})
      : super(key: key);

  @override
  _ShoppingListViewState createState() =>
      _ShoppingListViewState(documentID, owner, curentUser);
}

class _ShoppingListViewState extends State<ShoppingListView> {
  String documentID;
  String owner;
  String curentUser;

  _ShoppingListViewState(this.documentID, this.owner, this.curentUser);
  bool _isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            SvgPicture.asset(
              "lib/assets/images/banner.svg",
              width: MediaQuery.of(context).size.width,
            ),
            SafeArea(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton.icon(
                        icon: Icon(Icons.arrow_back,
                            color: Colors.white, size: 30),
                        label: Text(""),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      FlatButton.icon(
                        icon: Icon(Icons.input, color: Colors.white, size: 30),
                        label: Text(""),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) => SignOutDialog(
                              primaryButtonText: "NO",
                              title: "Sign Out",
                              secondaryButtonText: "SIGN OUT",
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 50.0),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text(
                          documentID,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 38.0,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.10),
                  Expanded(
                    child: StreamBuilder(
                        stream: Firestore.instance
                            .collection("shopingLists")
                            .document(widget.documentID)
                            .collection("items")
                            .snapshots(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Text("Nalagam...");
                          } else {
                            return ListView.builder(
                                itemCount: snapshot.data.documents.length,
                                itemBuilder: (context, index) {
                                  DocumentSnapshot docsSnap =
                                      snapshot.data.documents[index];

                                  return GestureDetector(
                                    onTap: () async {
                                      String curentUser = await getCurentUser();
                                      String ownerUser =
                                          await FirestoreService()
                                              .getOwnerUID(documentID);

                                      if (curentUser == ownerUser) {
                                        print("object");
                                      }
                                      // print(documentID);
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Dismissible(
                                        key: UniqueKey(),
                                        onDismissed: (direction) {
                                          print(docsSnap.data["isChecked"]);
                                          FirestoreService().deleteItem(
                                            widget.documentID,
                                            docsSnap.documentID,
                                          );
                                        },
                                        child: Container(
                                            child: Column(
                                          children: <Widget>[
                                            ItemTile(
                                              docsSnap.documentID,
                                              widget.documentID,
                                              docsSnap.data["isChecked"],
                                            ),
                                          ],
                                        )),
                                      ),
                                    ),
                                  );
                                });
                          }
                        }),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: Container(
        width: MediaQuery.of(context).size.width * 0.92,
        child: buildButtonsRow(context),
      ),
    );
  }

  Row buildButtonsRow(BuildContext context) {
    if (owner != curentUser) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          new FloatingActionButton(
            heroTag: null,
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) => AddDialogBox(
                  primaryButtonText: "ADD",
                  title: "Add Items",
                  secondaryButtonText: "CLOSE",
                  documentID: documentID,
                ),
              );
            },
            child: new Icon(Icons.add),
            backgroundColor: const Color(0xffFABC51),
          ),
        ],
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new FloatingActionButton(
            onPressed: () {
              print(documentID);
              showDialog(
                context: context,
                builder: (BuildContext context) => AddUser(documentID),
              );
            },
            child: new Icon(Icons.person_outline),
            backgroundColor: const Color(0xffFABC51),
          ),
          new FloatingActionButton(
            heroTag: null,
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) => AddDialogBox(
                  primaryButtonText: "ADD",
                  title: "Add Items",
                  secondaryButtonText: "CLOSE",
                  documentID: documentID,
                ),
              );
            },
            child: new Icon(Icons.add),
            backgroundColor: const Color(0xffFABC51),
          ),
        ],
      );
    }
  }

  void toggleFavorite() {
    setState(() {
      if (_isChecked) {
        _isChecked = false;
      } else {
        _isChecked = true;
      }
    });
  }
}
