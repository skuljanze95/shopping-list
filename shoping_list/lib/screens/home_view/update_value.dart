import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ItemTile extends StatefulWidget {
  String parentDocumentID;
  String documentID;
  bool isChecked;

  ItemTile(
    this.documentID,
    this.parentDocumentID,
    this.isChecked,
  );

  @override
  _ItemTileState createState() => _ItemTileState(
        documentID,
        parentDocumentID,
        isChecked,
      );
}

class _ItemTileState extends State<ItemTile> {
  String documentID;
  String parentDocumentID;
  bool isChecked;

  _ItemTileState(
    this.documentID,
    this.parentDocumentID,
    this.isChecked,
  );

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onDoubleTap: () {
        updateValue(parentDocumentID, documentID);
      },
      child: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
          color:
              (isChecked ? const Color(0xffA2FFCE) : const Color(0xffF9F9F9)),
          borderRadius: BorderRadius.circular(25),
        ),
        height: 80.0,
        width: MediaQuery.of(context).size.width,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 28.0),
              child: Text(
                documentID,
                style: TextStyle(fontSize: 24),
              ),
            ),
          ],
        ),
      ),
    );
  }

  updateValue(String documentID, itemID) async {
    if (isChecked == true) {
      Firestore.instance
          .collection('shopingLists')
          .document(documentID)
          .collection("items")
          .document(itemID)
          .setData({'isChecked': false});
    } else {
      Firestore.instance
          .collection('shopingLists')
          .document(documentID)
          .collection("items")
          .document(itemID)
          .setData({'isChecked': true});
    }
  }
}
