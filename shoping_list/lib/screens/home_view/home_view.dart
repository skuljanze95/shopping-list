import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shoping_list/screens/home_view/list_view.dart';

import 'package:shoping_list/services/firestore_services.dart';
import 'package:shoping_list/widgets/addList_dialog_box.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shoping_list/widgets/provider.dart';
import 'package:shoping_list/widgets/signOut_dialog_box.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            SvgPicture.asset(
              "lib/assets/images/banner.svg",
              width: MediaQuery.of(context).size.width,
            ),
            SafeArea(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton.icon(
                        icon: Icon(
                          Icons.input,
                          size: 30.0,
                          color: Colors.white,
                        ),
                        label: Text(""),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) => SignOutDialog(
                              primaryButtonText: "NO",
                              title: "Sign Out",
                              secondaryButtonText: "SIGN OUT",
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 50.0),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text(
                          "Welcome back",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 38.0,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.10),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text(
                          "Your shopping lists",
                          style: TextStyle(
                              color: const Color(0xff332F2F),
                              fontSize: 20,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    child: FutureBuilder(
                      future: FirebaseAuth.instance.currentUser(),
                      builder: (context, AsyncSnapshot<FirebaseUser> snapshot) {
                        if (snapshot.hasData) {
                          String userID = snapshot.data.uid;
                          return StreamBuilder(
                              stream: Firestore.instance
                                  .collection("shopingLists")
                                  .where("Users", arrayContains: userID)
                                  .snapshots(),
                              builder: (context, snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                      color: Colors.white,
                                      height:
                                          MediaQuery.of(context).size.height,
                                      width: MediaQuery.of(context).size.width,
                                      child: Text("Loading..."));
                                } else {
                                  return ListView.builder(
                                      itemCount: snapshot.data.documents.length,
                                      itemBuilder: (context, index) {
                                        DocumentSnapshot docsSnap =
                                            snapshot.data.documents[index];

                                        return GestureDetector(
                                          onTap: () async {
                                            String _curentUser =
                                                await Provider.of(context)
                                                    .auth
                                                    .getCurentUser();

                                            Navigator.of(context).push(
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    ShoppingListView(
                                                  documentID:
                                                      docsSnap.documentID,
                                                  owner: docsSnap["Owner"],
                                                  curentUser: _curentUser,
                                                ),
                                              ),
                                            );
                                          },
                                          child: Dismissible(
                                            key: UniqueKey(),
                                            onDismissed: (direction) async {
                                              String ownerUser =
                                                  await FirestoreService()
                                                      .getOwnerUID(
                                                          docsSnap.documentID);

                                              if (ownerUser == userID) {
                                                FirestoreService().deleteList(
                                                    docsSnap.documentID);
                                              } else {
                                                FirestoreService()
                                                    .deleteListUser(
                                                        docsSnap.documentID,
                                                        userID);
                                              }
                                            },
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: Container(
                                                height: 80.0,
                                                decoration: BoxDecoration(
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.black12,
                                                      blurRadius: 10.0,
                                                      offset: const Offset(
                                                          0.0, 10.0),
                                                    ),
                                                  ],
                                                  color:
                                                      const Color(0xffF9F9F9),
                                                  borderRadius:
                                                      BorderRadius.circular(25),
                                                ),
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 28.0),
                                                      child: Text(
                                                        docsSnap.documentID,
                                                        style: TextStyle(
                                                            fontSize: 24),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      });
                                }
                              });
                        } else {
                          return Container(
                              color: Colors.white,
                              height: MediaQuery.of(context).size.height,
                              width: MediaQuery.of(context).size.width,
                              child: Text("Loading..."));
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () async {
          FirebaseUser user = await FirebaseAuth.instance.currentUser();

          showDialog(
              context: context,
              builder: (BuildContext context) => AddListDialog(
                    primaryButtonText: "ADD",
                    title: "Add Lists",
                    secondaryButtonText: "CLOSE",
                    user: user.uid,
                    name: user.displayName,
                  ));
        },
        child: new Icon(Icons.add),
        backgroundColor: const Color(0xffFABC51),
      ),
    );
  }
}
