import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:shoping_list/services/firestore_services.dart';
import 'package:shoping_list/widgets/search_bar.dart';
import 'package:shoping_list/widgets/search_bar_users.dart';

class AddUser extends StatefulWidget {
  String documentID;

  AddUser(this.documentID);
  @override
  _AddUserState createState() => _AddUserState(documentID);
}

class _AddUserState extends State<AddUser> {
  final primaryColor = const Color(0xffFABC51);
  final formKey = GlobalKey<FormState>();
  final String documentID;

  String search_bar = "@";

  callback(search_string) {
    setState(() {
      search_bar = search_string;
    });
  }

  _AddUserState(this.documentID);

  @override
  Widget build(BuildContext context) {
    return (Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: primaryColor,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black38,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ]),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 24.0),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      AutoSizeText(
                        "Manege Users",
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 26,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: AutoSizeText(
                          "Duble tap to add users and swipe to delete",
                          maxLines: 2,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10.0),
                SearchBarUsers(search_bar, callback),
                SizedBox(height: 4.0),
                Container(
                  child: StreamBuilder(
                    stream: Firestore.instance
                        .collection("shopingLists")
                        .document(documentID)
                        .collection("Users")
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return Text("Nalagam...");
                      } else {
                        return ListView.builder(
                            shrinkWrap: true,
                            itemCount: snapshot.data.documents.length,
                            itemBuilder: (context, index) {
                              DocumentSnapshot docsSnap =
                                  snapshot.data.documents[index];
                              return GestureDetector(
                                onTap: () {},
                                child: Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(8, 5, 8, 5),
                                  child: Dismissible(
                                    key: UniqueKey(),
                                    onDismissed: (direction) {
                                      print(documentID);
                                      print(docsSnap.documentID);
                                      FirestoreService().deleteListUser(
                                          documentID, docsSnap.documentID);
                                    },
                                    child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          color: Colors.white,
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: AutoSizeText(
                                              docsSnap.data["Name"]),
                                        )),
                                  ),
                                ),
                              );
                            });
                      }
                    },
                  ),
                ),
                SizedBox(height: 4.0),
                Container(
                  height: 1,
                  color: Colors.white,
                  margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    StreamBuilder(
                      stream: Firestore.instance
                          .collection("Users")
                          .where("Email", isGreaterThanOrEqualTo: search_bar)
                          .where("Email", isLessThanOrEqualTo: search_bar + "x")
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Text("Nalagam...");
                        } else {
                          return ListView.builder(
                              shrinkWrap: true,
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, index) {
                                DocumentSnapshot docsSnap =
                                    snapshot.data.documents[index];
                                return GestureDetector(
                                  onDoubleTap: () {
                                    FirestoreService().addUser(
                                        documentID,
                                        docsSnap.data["uID"],
                                        docsSnap.data["Name"]);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white,
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            8.0, 5.0, 5.0, 5.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            AutoSizeText(
                                              docsSnap.data["Name"],
                                            ),
                                            AutoSizeText(
                                                docsSnap.data["Email"]),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              });
                        }
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
