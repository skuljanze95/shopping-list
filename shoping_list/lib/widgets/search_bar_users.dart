import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchBarUsers extends StatefulWidget {
  String search_bar;
  Function(String) callback;

  SearchBarUsers(this.search_bar, this.callback);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBarUsers> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.fromLTRB((width * 0.03), 0, (width * 0.03), 0),
        child: TextField(
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              prefixIcon: Icon(
                Icons.mail_outline,
                color: Colors.white,
              ),
              contentPadding: EdgeInsets.fromLTRB(0, 15.0, 0, 15.0),
              hintText: "Email",
              hintStyle: TextStyle(color: Colors.white)),
          onChanged: (text) {
            text.isEmpty
                ? widget.callback("@")
                : widget.callback('${text[0]}${text.substring(1)}');
          },
        ),
      ),
    );
  }
}
