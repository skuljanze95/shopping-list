import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';

class DialogBox extends StatelessWidget {
  final primaryColor = const Color(0xffFABC51);
  final String title,
      description,
      primaryButtonText,
      primaryButtonRout,
      secondaryButtonText,
      secondaryButtonRout;

  DialogBox({
    @required this.title,
    @required this.description,
    @required this.primaryButtonText,
    @required this.primaryButtonRout,
    this.secondaryButtonText,
    this.secondaryButtonRout,
  });

  @override
  Widget build(BuildContext context) {
    return (Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: primaryColor,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black38,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ]),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 24.0),
                AutoSizeText(
                  title,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                  ),
                ),
                SizedBox(height: 24.0),
                AutoSizeText(
                  description,
                  maxLines: 4,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                ),
                SizedBox(height: 24.0),
                RaisedButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context)
                        .pushReplacementNamed(primaryButtonRout);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                        constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width * 0.7,
                        ),
                        child: AutoSizeText(
                          primaryButtonText,
                          maxLines: 1,
                          style: TextStyle(color: primaryColor, fontSize: 22),
                        )),
                  ),
                ),
                SizedBox(height: 14.0),
                showSecondaryButton(context),
              ],
            ),
          ),
        ],
      ),
    ));
  }

  showSecondaryButton(BuildContext context) {
    if (secondaryButtonRout != null && secondaryButtonText != null) {
      return FlatButton(
        onPressed: () {
          Navigator.of(context).pop();
          Navigator.of(context).pushReplacementNamed(secondaryButtonRout);
        },
        child: Text(
          secondaryButtonText,
          style: TextStyle(color: Colors.white, fontSize: 22),
        ),
      );
    } else {
      return SizedBox(height: 15.0);
    }
  }
}
