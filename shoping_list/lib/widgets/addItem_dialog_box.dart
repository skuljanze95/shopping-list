import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:shoping_list/services/firestore_services.dart';

class AddDialogBox extends StatefulWidget {
  final String title, primaryButtonText, secondaryButtonText, documentID;

  AddDialogBox({
    @required this.title,
    @required this.primaryButtonText,
    @required this.secondaryButtonText,
    @required this.documentID,
  });

  @override
  _AddDialogBoxState createState() => _AddDialogBoxState(documentID);
}

class _AddDialogBoxState extends State<AddDialogBox> {
  final primaryColor = const Color(0xffFABC51);
  final formKey = GlobalKey<FormState>();
  final String documentID;

  String _item;

  _AddDialogBoxState(this.documentID);

  void submit() async {
    final form = formKey.currentState;
    form.save();
    FirestoreService().createItem(documentID, _item);
    formKey.currentState.reset();
  }

  @override
  Widget build(BuildContext context) {
    return (Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: primaryColor,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black38,
                    blurRadius: 10.0,
                    offset: const Offset(0.0, 10.0),
                  ),
                ]),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 10.0),
                AutoSizeText(
                  widget.title,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 26,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(height: 30.0),
                Form(
                  key: formKey,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 9.0),
                      child: TextFormField(
                        style: TextStyle(fontSize: 20),
                        decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0xff858585)),
                            ),
                            prefixIcon: Icon(
                              Icons.playlist_add,
                              color: const Color(0xFF858585),
                            )),
                        onSaved: (value) => _item = value,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        widget.secondaryButtonText,
                        style: TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    ),
                    RaisedButton(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30.0)),
                      ),
                      onPressed: () {
                        submit();
                      },
                      child: Container(
                          constraints: BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width * 0.7,
                          ),
                          child: AutoSizeText(
                            widget.primaryButtonText,
                            maxLines: 1,
                            style: TextStyle(color: primaryColor, fontSize: 22),
                          )),
                    ),
                  ],
                ),
                SizedBox(height: 10.0),
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
