import 'package:cloud_firestore/cloud_firestore.dart';

class FirestoreService {
  createlist(String listID, user, String name) {
    Firestore.instance.collection('shopingLists').document(listID).setData({
      "Users": [user],
      "Owner": user
    });
  }

  createItem(String documentID, itemID) {
    Firestore.instance
        .collection('shopingLists')
        .document(documentID)
        .collection("items")
        .document(itemID)
        .setData({'isChecked': false});
  }

  deleteItem(String documentID, itemID) {
    Firestore.instance
        .collection('shopingLists')
        .document(documentID)
        .collection("items")
        .document(itemID)
        .delete();
  }

  deleteList(String documentID) {
    Firestore.instance.collection('shopingLists').document(documentID).delete();
  }

  deleteListUser(String documentID, userID) {
    Firestore.instance
        .collection('shopingLists')
        .document(documentID)
        .updateData({
      "Users": FieldValue.arrayRemove([userID])
    });
    Firestore.instance
        .collection('shopingLists')
        .document(documentID)
        .collection("Users")
        .document(userID)
        .delete();
  }

  getOwnerUID(String listID) async {
    var data1 = await Firestore.instance
        .collection('shopingLists')
        .document(listID)
        .get();

    return (data1.data["Owner"]);
  }

  addUser(String listID, user, name) {
    Firestore.instance
        .collection('shopingLists')
        .document(listID)
        .collection("Users")
        .document("$user")
        .setData({"uID": user, "Name": name});

    Firestore.instance.collection('shopingLists').document(listID).updateData({
      "Users": FieldValue.arrayUnion([user])
    });
  }


  

}
