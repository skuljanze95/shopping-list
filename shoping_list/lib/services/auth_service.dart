import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Stream<String> get onAuthStateChanged => _firebaseAuth.onAuthStateChanged.map(
        (FirebaseUser user) => user?.uid,
      );

//Email & Password Sing Up

  Future<String> createUserWithEmailAndPassword(
      String email, String password, String name) async {
    final authResult = await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );

    // Update the username
    await updateUserName(name, authResult.user);
    await createUserInFirestore(
        authResult.user.uid, name, authResult.user.email);
    return authResult.user.uid;
  }

  Future updateUserName(String name, FirebaseUser currentUser) async {
    var userUpdateInfo = UserUpdateInfo();

    userUpdateInfo.displayName = name;
    await currentUser.updateProfile(userUpdateInfo);
    await currentUser.reload();
  }

  // Email & Password Sign In
  Future<String> signInWithEmailAndPassword(
      String email, String password) async {
    return (await _firebaseAuth.signInWithEmailAndPassword(
            email: email, password: password))
        .user
        .uid;
  }

  // Sign Out
  signOut() {
    return _firebaseAuth.signOut();
  }

  Future getUser() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();

    return user;
  }

  Future getCurentUser() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();

    String curentUser = user.uid;

    return curentUser;
  }
}

Future getCurentUser() async {
  FirebaseUser user = await FirebaseAuth.instance.currentUser();

  String curentUser = user.uid;

  return curentUser;
}

createUserInFirestore(String uID, name, email) {
  Firestore.instance
      .collection('Users')
      .document(uID)
      .setData({"uID": uID, "Name": name, "Email": email});
}
